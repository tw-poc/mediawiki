This is MediaWiki version : 1.35.xx

Project Structure:

1. **./project_root** = Application core files
2. **./bin/*.sh** = Helper scripts for deployment.
3. **./bin/*yaml** = MediaWiki Kubernetes deployment yaml files.
4. **.*sh** = Docker image build helper files.
5. **Dockerfile** = Dockerfile for the MediaWiki Application. It uses an upstream MediaWiki Docker image and on top up that I've added my customizations. 
6. ***Jenkinsfile** = Jenkinsfiles for build & deployments.
7. **project.properties** = Property file contains project version
