#!/bin/bash
PROJECT_VERSION=$(cat project.properties)
APP_VERSION=$PROJECT_VERSION"_"$BUILD_NUMBER

# Build docker image
docker build -t mediawiki:$APP_VERSION .

BUILD_STATUS=$(echo $?)

if [ $BUILD_STATUS != 0 ]
then
 echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
 echo "Docker image build failed !!! Please troubleshoot further"
 exit 1
 else
 echo "Docker image has been built successfully"
 
fi
