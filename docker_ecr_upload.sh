#!/bin/bash
PROJECT_VERSION=$(cat project.properties)
APP_VERSION=$PROJECT_VERSION"_"$BUILD_NUMBER

#Get ECR Token
ECR_TOKEN=$(aws ecr get-login --region ap-south-1 | awk {'print $6'})

#ECR Login
docker login -u AWS -p $ECR_TOKEN https://178672324164.dkr.ecr.ap-south-1.amazonaws.com

# Tag image Tag and upload to ECR
docker tag mediawiki:$APP_VERSION 178672324164.dkr.ecr.ap-south-1.amazonaws.com/mediawiki:$APP_VERSION
docker push 178672324164.dkr.ecr.ap-south-1.amazonaws.com/mediawiki:$APP_VERSION

BUILD_STATUS=$(echo $?)

if [ $BUILD_STATUS != 0 ]
then
 echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
 echo "Docker image ECR upload failed !!! Please troubleshoot further"
 exit 1
 else

 echo "Docker image version $APP_VERSION has been uploaded to ECR successfully"
 
fi

