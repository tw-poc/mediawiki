#!/bin/bash

# Replacing new APP_VERSION
sed -i 's/APP_VERSION/'"$docker_version"'/g' mediawiki-deployment.yaml

# Deleting existing deployment
/usr/local/bin/kubectl delete deployment mediawiki-deployment || true
sleep 20

# Creating new deployment
/usr/local/bin/kubectl create -f mediawiki-deployment.yaml
