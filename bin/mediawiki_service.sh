#!/bin/bash

# Deleting existing Service
/usr/local/bin/kubectl delete service mediawiki-service || true
sleep 20

# Creating new Service
/usr/local/bin/kubectl create -f mediawiki-service.yaml
